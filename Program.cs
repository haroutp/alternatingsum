﻿using System;

namespace AlternatingSum
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        static int[] alternatingSums(int[] a) {
            int even = 0;
            int odd = 0;
            
            for(int i = 0; i < a.Length; i++){
                if(i % 2 == 0){
                    even += a[i];
                }else{
                    odd += a[i];
                }
            }
            
            int[] newA = new int[2]{even, odd};
            return newA;
        }

    }
}
